#
# language specific date formatting config below (default is yyyy-mm-dd).
#
DESTDIR=
DESTPATH = usr/share/doc
# Use Make internal functions 'subst' and 'wildcard'; (from right to left):
# get list of all PO files in dir, first substitute the extension with nothing,
# then do so for the manual name to get the list of languages.
LANGUAGES = $(subst $(name).,,$(subst .po,,$(wildcard *.po)))
# Program name and option
DBTOEPUB = dbtoepub
XP = xsltproc --nonet --novalid --xinclude ../common/html.xsl
DBLATEX = SOURCE_DATE_EPOCH=$$SOURCE_DATE_EPOCH SOURCE_DATE_EPOCH_TEX_PRIMITIVES=1 dblatex "--tmpdir=texdir" -T native -b xetex -p ../common/dblatex.xsl
# Language specific date formatting
DATE_00 = $(shell dpkg-parsechangelog -l ../../debian/changelog -S Date | date -u "+%Y"-"%m"-"%d" -f -)
DATE_01 = $(shell dpkg-parsechangelog -l ../../debian/changelog -S Date | date -u "+%d"."%m"."%Y" -f -)
DATE_02 = $(shell dpkg-parsechangelog -l ../../debian/changelog -S Date | date -u "+%d"-"%m"-"%Y" -f -)
DATE_03 = $(shell dpkg-parsechangelog -l ../../debian/changelog -S Date | date -u "+%Y"年"%m"月"%d"日 -f -)
DATE_04 = $(shell dpkg-parsechangelog -l ../../debian/changelog -S Date | date -u "+%d"/"%m"/"%Y" -f -)
DATE_05 = $(shell dpkg-parsechangelog -l ../../debian/changelog -S Date | date -u "+%Y"/"%-m"/"%-d" -f -)
SED_00= 's%</para><para> </para>%$(DATE_00)%'
SED_01= 's%</para><para> </para>%$(DATE_01)%'
SED_02= 's%</para><para> </para>%$(DATE_02)%'
SED_03= 's%</para><para> </para>%$(DATE_03)%'
SED_04= 's%</para><para> </para>%$(DATE_04)%'
SED_05= 's%</para><para> </para>%$(DATE_05)%'
# Common para tag fix (after the date has been included)
SED_100= 's%<subtitle><para>%<subtitle>%'
# Fix for id tag (legacy manuals)
SED_101= "s%id='[0-9]\+%id='%g"
SED_102= "s% id=''%%g"

# Use Make internal function 'subst': substitute -manual with nothing to get
# the directory name.
directory = $(subst -manual,,$(name))

all: build

update:
	if [ "$(legacy)" = "yes" ] ; then \
		echo "Not updating, $(name) is a legacy manual." ; \
	else \
		../scripts/get_manual_version ; \
		../scripts/get_manual ; \
		po4a --no-translations --msgmerge-opt --no-location po4a.cfg ; \
		msgcat --no-location -o $(name).pot $(name).pot ; \
	fi

update-copyright:
	../scripts/get_copyright $(path1) $(name)

readme:
	sed -e 's,##URL##,$(url),g' -e 's,##NAME##,$(name),g' -e "s,##DIR##,$(directory),g" \
	       ../common/README.common-translations > README.$(name)-translations

status:
	@for f in *.po ; do \
		LANG=C msgfmt -vv $$f -o /dev/null ; \
	done
	@cat fixme-status.txt

build: build-html build-epub

ifndef LINGUA
build-xml:
	# create localized XML files
	po4a --msgmerge-opt --no-location po4a.cfg
	# rewrap the PO file just to be sure
	msgcat --no-location -o $(name).pot $(name).pot
	# special case English master file
	cp $(name).xml $(name).en.xml
	# modify XML file
	for f in en $(LANGUAGES) ; do \
		if [ -e $(name).$$f.xml ] ; then \
			echo "Creating XML for $$f"; \
			case "$$f" in \
				cs|de|da|nb-no|pl|ro|uk) sed -i $(SED_01) $(name).$$f.xml \
				;; \
				fr|nl) sed -i $(SED_02) $(name).$$f.xml \
				;; \
				ja) sed -i $(SED_03) $(name).$$f.xml \
				;; \
				es|it|pt-br|pt-pt) sed -i $(SED_04) $(name).$$f.xml \
				;; \
				zh-tw) sed -i $(SED_05) $(name).$$f.xml \
				;; \
				*) sed -i $(SED_00) $(name).$$f.xml \
				;; \
			esac ; \
			sed -i $(SED_100) $(name).$$f.xml ; \
			sed -i $(SED_101) $(name).$$f.xml ; \
			sed -i $(SED_102) $(name).$$f.xml ; \
		fi ; \
	done
else
build-xml:
	# create the localized XML file
	po4a --translate-only $(name).$(LINGUA).xml po4a.cfg
	# rewrap the PO file just to be sure
	msgcat -o $(name).$(LINGUA).po $(name).$(LINGUA).po
	# modify localized XML file
	if [ -e $(name).$(LINGUA).xml ] ; then \
		case "$(LINGUA)" in \
			cs|de|da|nb-no|pl|ro|uk) sed -i $(SED_01) $(name).$(LINGUA).xml \
			;; \
			fr|nl) sed -i $(SED_02) $(name).$(LINGUA).xml \
			;; \
			ja) sed -i $(SED_03) $(name).$(LINGUA).xml \
			;; \
			es|it|pt-br|pt-pt) sed -i $(SED_04) $(name).$(LINGUA).xml \
			;; \
			zh-tw) sed -i $(SED_05) $(name).$$f.xml \
			;; \
			*) sed -i $(SED_00) $(name).$(LINGUA).xml \
			;; \
		esac ; \
		sed -i $(SED_100) $(name).$(LINGUA).xml ; \
		sed -i $(SED_101) $(name).$(LINGUA).xml ; \
		sed -i $(SED_102) $(name).$(LINGUA).xml ; \
	fi
endif

ifndef LINGUA
build-html: build-xml
	# build HTML version and restore internal links for all languages
	for f in en $(LANGUAGES) ; do \
		echo "Creating HTML for $$f"; \
		if [ -e $(name).$$f.xml ] ; then \
			$(XP) $(name).$$f.xml && mv index.html $(name).$$f.html ; \
			sed -i "s/href=\"index.html/href=\"$(name).$$f.html/g" $(name).$$f.html ; \
		fi ; \
	done
else
build-html: build-xml
	# build the HTML file and restore internal links
	if [ -e $(name).$(LINGUA).xml ] ; then \
		$(XP) $(name).$(LINGUA).xml && mv index.html $(name).$(LINGUA).html ; \
		sed -i "s/href=\"index.html/href=\"$(name).$(LINGUA).html/g" $(name).$(LINGUA).html ; \
	fi
endif

ifndef LINGUA
build-epub: build-xml
	# build EPUB version for all languages
	for f in en $(LANGUAGES) ; do \
		echo "Creating EPUB for $$f"; \
		mkdir images-tmp ; \
		cp images/*.* images-tmp/ ; \
		if [ -e images/$$f ] ; then \
		    cp -v images/$$f/*.* images-tmp/ ; \
		fi ; \
		if [ -e $(name).$$f.xml ] ; then \
			sed -i "s#./images#./images-tmp#g" $(name).$$f.xml ; \
			$(DBTOEPUB) $(name).$$f.xml ; \
		fi ; \
		rm -rf images-tmp ; \
	done
else
build-epub: build-xml
	mkdir images-tmp
	cp images/*.* images-tmp/
	if [ -e images/$(LINGUA) ] ; then \
	    cp -v images/$(LINGUA)/*.* images-tmp/ ; \
	fi
	if [ -e $(name).$(LINGUA).xml ] ; then \
		sed -i "s#./images#./images-tmp#g" $(name).$(LINGUA).xml ; \
		$(DBTOEPUB) $(name).$(LINGUA).xml ; \
	fi
	rm -rf images-tmp
endif

ifndef LINGUA
pdf: build-xml
	sed -i $(SED_00) $(name).en.xml
	sed -i $(SED_100) $(name).en.xml
	$(DBLATEX) $(name).en.xml --param=lingua=en
else
pdf: build-xml
	if [ -e $(name).$(LINGUA).xml ] ; then \
		$(DBLATEX) $(name).$(LINGUA).xml --param=lingua=$(LINGUA) ; \
	fi
endif

ifndef LINGUA
xml: build-xml
else
xml: build-xml
endif

ifndef LINGUA
html: build-html
else
html: build-html
endif

ifndef LINGUA
epub: build-epub
else
epub: build-epub
endif

install: build
	# en needs to be first
	#
	# the first two epub related sed commands further below are needed to get
	# rid of any environmental influence at package generation time. This concerns
	# the content identifier in both .opf and .ncx files. While these sed commands
	# are doing the right thing for archs amd64 and arm64, the set of the next
	# two sed commands are needed for i386 and armhf instead due to a different
	# content identifier string.
	for f in en $(LANGUAGES) ; do \
		if [ "$(name)" = "audacity-manual" ] || [ "$(name)" = "rosegarden-manual" ] || \
			[ "$(name)" = "debian-edu-itil-manual" ]; then \
			export LANGPATH=$(DESTDIR)/debian-edu-doc-legacy-$$f/$(DESTPATH)/debian-edu-doc-legacy-$$f ; \
		else \
			export LANGPATH=$(DESTDIR)/debian-edu-doc-$$f/$(DESTPATH)/debian-edu-doc-$$f ; \
		fi ; \
		mkdir -p $$LANGPATH ; \
		if [ -e $(name).$$f.html ] ; then \
			cp $(name).$$f.html $$LANGPATH/$(name).html ; \
			sed -i "s/href=\"$(name).$$f.html/href=\"$(name).html/g" $$LANGPATH/$(name).html ; \
			sed -i "s#./images#./$(name)-images#g" $$LANGPATH/$(name).html ; \
			if [ "$$f" = "en" ] ; then \
			  mkdir -p $$LANGPATH/$(name)-images/ ; \
			  find images -maxdepth 1 -type f -exec cp \{\} $$LANGPATH/$(name)-images/ \; ; \
			  sed -i $(SED_00) $(name).xml ; \
			  sed -i $(SED_100) $(name).xml ; \
			  $(DBLATEX) -o $$LANGPATH/$(name).pdf $(name).xml --param=lingua=$$f ; \
			else \
				mkdir -p $$LANGPATH/$(name)-images ; \
				mkdir -p $$LANGPATH/$(name)-images-tmp ; \
				cd images ; \
				for i in *.* ; do \
					if [ -e $$f/$$i ] ; then \
						cp $$f/$$i $$LANGPATH/$(name)-images-tmp/ ; \
					else \
						case "$(name)" in \
							audacity-manual|debian-edu-itil-manual|rosegarden-manual) \
								ln -sf ../../debian-edu-doc-legacy-en/$(name)-images/$$i $$LANGPATH/$(name)-images/$$i \
							;; \
							*) ln -sf ../../debian-edu-doc-en/$(name)-images/$$i $$LANGPATH/$(name)-images/$$i \
							;; \
						esac ; \
						cp $$i $$LANGPATH/$(name)-images-tmp/ ; \
					fi ; done ; \
				cd .. ; \
				sed -i "s#./images#./$(name)-images#g" $(name).$$f.xml ; \
				$(DBLATEX) -I $$LANGPATH/ -I $(DESTDIR)/debian-edu-doc-en/$(DESTPATH)/debian-edu-doc-en/ -o $$LANGPATH/$(name).pdf $(name).$$f.xml --param=lingua=$$f ; \
				cp $$LANGPATH/$(name)-images-tmp/*.* $$LANGPATH/$(name)-images 2> /dev/null ; \
				rm -rf $$LANGPATH/$(name)-images-tmp ; \
			fi ; \
		fi ; \
		if [ -e $(name).epub ] && [ "$$f" = "en" ]  ; then \
			cp $(name).epub $$LANGPATH/$(name).epub ; \
		fi ; \
		if [ -e $(name).$$f.epub ] ; then \
			cp $(name).$$f.epub $$LANGPATH/$(name).epub ; \
			cd $$LANGPATH ; \
			unzip -q $(name).epub -d epub ; \
			sed -i 's#_idm[0-9]\+#_idm123#' epub/OEBPS/content.opf ; \
			sed -i 's#_idm[0-9]\+#_idm123#' epub/OEBPS/toc.ncx ; \
			sed -i 's#_idp[0-9]\+#_idp123#' epub/OEBPS/content.opf ; \
			sed -i 's#_idp[0-9]\+#_idp123#' epub/OEBPS/toc.ncx ; \
			find epub -exec touch -d @0 '{}' \; ; \
			rm $(name).epub ; \
			zip -q -r $(name).epub epub/ ; \
			rm -rf epub/ ; \
			cd - ; \
		fi ; \
	done

clean:
	rm -f *.pdf
	rm -f *.html
	rm -f $(name).*.xml
	rm -f *.epub
	rm -f *.pdf
	rm -f *.po~
	rm -rf images-tmp
	rm -f .collapsed.$(name).*.xml
